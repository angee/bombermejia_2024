<?php
class Bomberos  extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Bombero');

    }


    public function index()
	{
        // *********************** ACTIVIDAD 3 ****************************
        // Indicadores
        $data["List2020"] = $this->Bombero->getByTotalVisit20();
        $data["List2021"] = $this->Bombero->getByTotalVisit21();
        $data["List2022"] = $this->Bombero->getByTotalVisit22();
        $data["List2023"] = $this->Bombero->getByTotalVisit23();
        // Graficas
        $data["getByVisit2020"] = $this->Bombero->getByVisit20();
        $data["getByVisit2021"] = $this->Bombero->getByVisit21();
        $data["getByVisit2022"] = $this->Bombero->getByVisit22();
        $data["getByVisit2023"] = $this->Bombero->getByVisit23();

        // *********************** ACTIVIDAD 4 ****************************
        //codigo liz
        $data["bomberosByNotificacion"] = $this->Bombero->getByTotalNotification("desc", 10);
        $totalNotificaciones = $this->Bombero->getTotalNotificaciones();
        $data["totalNotificaciones"] = $totalNotificaciones;
        $data["totalNotificacionesTop10"] = $this->Bombero->getTotalNotificacionesTop10();
            // Calcular el total de estados
        $totalEstados = $this->Bombero->getTotalEstados();
        // Pasar el total de estados a la vista
        $data["totalEstados"] = $totalEstados;
        $data["bomberosByState"] = $this->Bombero->getByTotalState("desc");
        $data["bomberosByActividad"]=$this->Bombero->getByTotalActividad("desc",10);
        // Calcular el total de actividades
        $totalActividades = $this->Bombero->getTotalActividades();

        // Pasar el total de actividades a la vista
        $data["totalActividades"] = $totalActividades;

        $data["bomberosByMeses"]=$this->Bombero->getByTotalMes("desc");
        $data["totalSolicitudes"] = $this->Bombero->getTotalSolicitudes();

        // *********************** ACTIVIDAD 5 ****************************
        //codigo cintia
        $data['cantidadMensajesPorAnio'] = $this->Bombero->getCantidadMensajesPorAnio();
        $data['mensajesPorAnioYMes'] = $this->Bombero->getMensajesPorAnioYMes();
        $data['cantidadArchivosPorTipo'] = $this->Bombero->getCantidadArchivosPorTipo();
        $data['getTotalMensajes'] = $this->Bombero->getTotalMensajes();
        $data['getTotalArchivosYGaleriasSumados'] = $this->Bombero->getTotalArchivosYGaleriasSumados();
        $data['sumaTotalMensajes'] = $this->Bombero->getSumaTotalMensajesPorAnio();


        // *********************** ACTIVIDAD 6 ****************************
        //Indicadores
        $data["totalNotifications"] = $this->Bombero->getByTotalNotiForKPI6();
        $data["totalSolicitudes"] = $this->Bombero->getByTotalSoliForKPI6();
        $data["totalSolicitudes_62"] = $this->Bombero->getByTotalSoliForKPI_62();
        $data["totalNotificaciones_62"] = $this->Bombero->getByTotalNotiForKPI_62();

        $data["totalSolicitudes_63"] = $this->Bombero->getByTotalSoliForKPI_63();
        $data["totalNotificaciones_63"] = $this->Bombero->getByTotalNotiForKPI_63();

        $data["totalSolicitudes_64"] = $this->Bombero->getByTotalSoliForKPI_64();
        $data["totalNotificaciones_64"] = $this->Bombero->getByTotalNotiForKPI_64();

        //Graficas
        $data["dataForKPI6"] = $this->Bombero->getDataForKPI6();
        $data["dataForKPI_62"] = $this->Bombero->getDataForKPI_62();
        $data["dataForKPI_63"] = $this->Bombero->getDataForKPI_63();
        $data["dataForKPI_64"] = $this->Bombero->getDataForKPI_64();


		$this->load->view('header');
		$this->load->view('bomberos/index', $data);
        $this->load->view('footer');
	}

}
?>
