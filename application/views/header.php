<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bussines Intelligence</title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?php echo base_url('plantilla/vendors/typicons/typicons.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('plantilla/vendors/css/vendor.bundle.base.css'); ?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vertical-layout-light/style.css') ?>">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
  <!-- Importar CHART JS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@2.9.0/dist/chartjs-adapter-moment.bundle.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.0"></script>
  <script src="https://cdn.jsdelivr.net/npm/chartjs-3d@2.1.3"></script>
  <!-- Font Awesome -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">

</head>
<body>

  <div class="container-scroller">

        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-right" style="height: 80px; position: fixed; width: 100%; top: 0; background-color: #333; color: #fff; z-index: 1000;">
         <!-- <div class="navbar-menu-wrapper d-flex align-items-center justify-content-right"style="height: 80px;"> -->
           <ul class="navbar-nav mr-lg-2">
             <li class="nav-item ml-3">
               <h4 class="mb-0">Dashboard</h4>
             </li>
             <!-- <li class="nav-item">
               <div class="d-flex align-items-baseline">
                 <p class="mb-0">Home</p>
                 <i class="typcn typcn-chevron-right"></i>
                 <p class="mb-0">Main Dahboard</p>
               </div>
             </li> -->
           </ul>
         </div>

       <div class="container-fluid page-body-wrapper">
         <div class="theme-setting-wrapper">
           <div id="settings-trigger"><i class="typcn typcn-cog-outline"></i></div>
           <div id="theme-settings" class="settings-panel">
             <i class="settings-close typcn typcn-times"></i>
             <p class="settings-heading">SIDEBAR SKINS</p>
             <div class="sidebar-bg-options selected" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
             <div class="sidebar-bg-options" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
             <p class="settings-heading mt-2">HEADER SKINS</p>
             <div class="color-tiles mx-0 px-4">
               <div class="tiles success"></div>
               <div class="tiles warning"></div>
               <div class="tiles danger"></div>
               <div class="tiles info"></div>
               <div class="tiles dark"></div>
               <div class="tiles default"></div>
             </div>
           </div>
         </div>

         <div id="right-sidebar" class="settings-panel">
           <i class="settings-close typcn typcn-times"></i>
           <ul class="nav nav-tabs" id="setting-panel" role="tablist">
             <li class="nav-item">
               <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
             </li>
           </ul>
           <div class="tab-content" id="setting-content">
             <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
               <div class="add-items d-flex px-3 mb-0">
                 <form class="form w-100">
                   <div class="form-group d-flex">
                     <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                     <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
                   </div>
                 </form>
               </div>
               <div class="list-wrapper px-3">
                 <ul class="d-flex flex-column-reverse todo-list">
                   <li>
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox">
                         Team review meeting at 3.00 PM
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li>
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox">
                         Prepare for presentation
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li>
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox">
                         Resolve all the low priority tickets due today
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li class="completed">
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox" checked>
                         Schedule meeting for next week
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li class="completed">
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox" checked>
                         Project review
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                 </ul>
               </div>
               <div class="events py-4 border-bottom px-3">
                 <div class="wrapper d-flex mb-2">
                   <i class="typcn typcn-media-record-outline text-primary mr-2"></i>
                   <span>Feb 11 2018</span>
                 </div>
                 <p class="mb-0 font-weight-thin text-gray">Creating component page</p>
                 <p class="text-gray mb-0">build a js based app</p>
               </div>
               <div class="events pt-4 px-3">
                 <div class="wrapper d-flex mb-2">
                   <i class="typcn typcn-media-record-outline text-primary mr-2"></i>
                   <span>Feb 7 2018</span>
                 </div>
                 <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                 <p class="text-gray mb-0 ">Call Sarah Graves</p>
               </div>
             </div>
             <!-- To do section tab ends -->
             <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
               <div class="d-flex align-items-center justify-content-between border-bottom">
                 <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                 <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
               </div>
               <ul class="chat-list">
                 <li class="list active">
                   <div class="profile"><img src="images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Thomas Douglas</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">19 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                   <div class="info">
                     <div class="wrapper d-flex">
                       <p>Catherine</p>
                     </div>
                     <p>Away</p>
                   </div>
                   <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                   <small class="text-muted my-auto">23 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Daniel Russell</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">14 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                   <div class="info">
                     <p>James Richardson</p>
                     <p>Away</p>
                   </div>
                   <small class="text-muted my-auto">2 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Madeline Kennedy</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">5 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Sarah Graves</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">47 min</small>
                 </li>
               </ul>
             </div>
             <!-- chat tab ends -->
           </div>
         </div>
         <!-- partial -->
         <!-- partial:partials/_sidebar.html -->
         <nav id="sidebar" class="sidebar sidebar-offcanvas">
           <ul class="nav">
             <li class="nav-item">
               <a class="nav-link" href="<?php echo site_url();?>/bomberos/index">
                 <i class="typcn typcn-device-desktop menu-icon"></i>
                 <span class="menu-title">Dashboard</span>
                 <div class="badge badge-danger">new</div>
               </a>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                 <i class="typcn typcn-document-text menu-icon"></i>
                 <span class="menu-title">UI Elements</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="ui-basic">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                   <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Dropdowns</a></li>
                   <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
                 <i class="typcn typcn-film menu-icon"></i>
                 <span class="menu-title">Form elements</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="form-elements">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
                 <i class="typcn typcn-chart-pie-outline menu-icon"></i>
                 <span class="menu-title">Charts</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="charts">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="pages/charts/chartjs.html">ChartJs</a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-th-small-outline menu-icon"></i>
                 <span class="menu-title">Tables</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="pages/tables/basic-table.html">Basic table</a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#icons" aria-expanded="false" aria-controls="icons">
                 <i class="typcn typcn-compass menu-icon"></i>
                 <span class="menu-title">Icons</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="icons">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="pages/icons/mdi.html">Mdi icons</a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                 <i class="typcn typcn-user-add-outline menu-icon"></i>
                 <span class="menu-title">User Pages</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="auth">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Login </a></li>
                   <li class="nav-item"> <a class="nav-link" href="pages/samples/register.html"> Register </a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#error" aria-expanded="false" aria-controls="error">
                 <i class="typcn typcn-globe-outline menu-icon"></i>
                 <span class="menu-title">Error pages</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="error">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="pages/samples/error-404.html"> 404 </a></li>
                   <li class="nav-item"> <a class="nav-link" href="pages/samples/error-500.html"> 500 </a></li>
                 </ul>
               </div>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="https://bootstrapdash.com/demo/polluxui-free/docs/documentation.html">
                 <i class="typcn typcn-mortar-board menu-icon"></i>
                 <span class="menu-title">Documentation</span>
               </a>
             </li>
           </ul>
         </nav>
         <!-- partial -->
              <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
<style>

/* Estilos para el encabezado */
.navbar-menu-wrapper {
  height: 80px; /* Ajusta la altura según tus necesidades */
  position: fixed;
  top: 0;
  width: 100%;
  background-color: #333;
  color: #fff;
  z-index: 1000;
}

/* Estilos para la barra lateral */
#sidebar {
  position: fixed;
  top: 80px; /* Ajusta el espacio para que la barra no se sobreponga al encabezado */
  left: 0;
  height: 100%;
  overflow-y: auto;
  background-color: #fff;
  width: 250px;
  z-index: 1000;
  /* Agrega otros estilos que desees */
}

/* Estilos para el contenido principal */
.main-panel {
  margin-left: 250px; /* Ajusta este valor para que el contenido no se superponga con la barra lateral */
  padding-top: 100px; /* Ajusta este valor para dejar espacio entre el encabezado y el contenido */
  /* Agrega otros estilos que desees */
}


</style>
